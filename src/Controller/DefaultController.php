<?php
declare(strict_types = 1);

namespace App\Controller;

use App\Entity\User;
use App\Form\UserType;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Serializer\Exception\NotEncodableValueException;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;

/**
 * @Route("/")
 */
class DefaultController extends AbstractController
{

    /**
     * @var SerializerInterface
     */
    private $serializer;

    /**
     * @var ValidatorInterface
     */
    private $validator;

    /**
     * @var UserPasswordEncoderInterface
     */
    private $passwordEncoder;

    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * @var UserRepository
     */
    private $userRepository;

    /**
     * DefaultController constructor.
     * @param SerializerInterface $serializer
     * @param ValidatorInterface $validator
     * @param UserPasswordEncoderInterface $passwordEncoder
     * @param EntityManagerInterface $em
     * @param UserRepository $userRepository
     */
    public function __construct(
        SerializerInterface $serializer,
        ValidatorInterface $validator,
        UserPasswordEncoderInterface $passwordEncoder,
        EntityManagerInterface $em,
        UserRepository $userRepository
    )
    {
        $this->serializer = $serializer;
        $this->validator = $validator;
        $this->passwordEncoder = $passwordEncoder;
        $this->em = $em;
        $this->userRepository = $userRepository;
    }


    /**
     * @Route("/create-user", name="create_user")
     * @return Response
     */
    public function createUser():Response
    {

        $form = $this->createForm(UserType::class, null);

        return $this->render('default/createUser.html.twig', [
            'form' => $form->createView()
        ]);
    }

    /**
     * @param Request $request
     * @Route("/api/user/create", methods={"POST"}, name="api_user_create")
     * @return JsonResponse
     */
    public function create(Request $request):JsonResponse
    {

        $data = $request->getContent();

        try {
            /**
             * @var User $user
             */
            $user = $this->serializer->deserialize($data, User::class, 'json', ['groups' => ['create']]);
        }catch (NotEncodableValueException $exception){
            return new JsonResponse(
                ['k doreseni stejny format jako ma spatna validace dat....'],
                400,
                [],
                true
            );
        }

        $violations = $this->validator->validate($user);
        if (count($violations) > 0) {
            return new JsonResponse(
                $this->serializer->serialize($violations, 'json'),
                400,
                [],
                true
            );
        }

        $user->setPassword($this->passwordEncoder->encodePassword($user, $user->getTextPassword()));

        $this->em->persist($user);
        $this->em->flush();

        return new JsonResponse(
            $this->serializer->serialize($user, 'json', ['groups' => 'read']),
            201,
            [],
            true
        );
    }

    /**
     * @Route("/api/users", methods={"GET"}, name="api_user_list")
     * @return JsonResponse
     */
    public function list():JsonResponse
    {
        $users = $this->userRepository->findAll();

        return new JsonResponse(
            $this->serializer->serialize($users, 'json', [ 'groups' => 'read']),
            200,
            [],
            true
        );

    }
}
