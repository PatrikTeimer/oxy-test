<?php
declare(strict_types = 1);

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;


/**
 * @UniqueEntity("username", message="User is exist!")
 * @ORM\Entity(repositoryClass="App\Repository\UserRepository")
 */
class User implements UserInterface
{

    private const ROLES = ['USER', 'ADMIN'];

    /**
     * @var int
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @Groups({"read"})
     * @ORM\Column(type="integer", options={"unsigned"=true})
     */
    private $id;

    /**
     * @var string
     * @Assert\NotBlank
     * @Assert\Length(
     *  min = 5,
     *  max = 64,
     *  minMessage = "Your name must be at least {{ limit }} characters long",
     *  maxMessage = "Your name cannot be longer than {{ limit }} characters"
     * )
     * @Groups({"create", "read"})
     * @ORM\Column(type="string", length=64)
     */
    private $fullName;

    /**
     * @var string
     * @Assert\NotBlank
     * @Assert\Length(max = 64, maxMessage = "Your email cannot be longer than {{ limit }} characters")
     * @Assert\Email(message = "The email '{{ value }}' is not a valid email.")
     * @Groups({"create", "read"})
     * @ORM\Column(type="string", length=64, unique=true)
     */
    private $username;

    /**
     * @var string
     * @ORM\Column(type="string", length=128)
     */
    private $password;

    /**
     * @var string
     * @Groups({"create", "read"})
     * @Assert\NotBlank
     * @Assert\Choice(callback="getRoles")
     * @ORM\Column(type="string", length=16)
     */
    private $role;

    /**
     * @var string
     * @Assert\NotBlank
     * @Assert\Length(
     *  min = 5,
     *  max = 16,
     *  minMessage = "Your password be at least {{ limit }} characters long",
     *  maxMessage = "Your password be longer than {{ limit }} characters"
     * )
     * @Groups({"create"})
     */
    private $textPassword;


    /**
     * User constructor.
     */
    public function __construct()
    {
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getFullName(): string
    {
        return $this->fullName;
    }

    /**
     * @param string $fullName
     */
    public function setFullName(string $fullName): void
    {
        $this->fullName = $fullName;
    }

    /**
     * @return string
     */
    public function getUsername(): string
    {
        return $this->username;
    }

    /**
     * @param string $username
     */
    public function setUsername(string $username): void
    {
        $this->username = $username;
    }

    /**
     * @return string
     */
    public function getPassword(): string
    {
        return $this->password;
    }

    /**
     * @param string $password
     */
    public function setPassword(string $password): void
    {
        $this->password = $password;
        $this->textPassword = null;
    }

    /**
     * @return string
     */
    public function getRole(): string
    {
        return $this->role;
    }

    /**
     * @param string $role
     */
    public function setRole(string $role): void
    {
        $this->role = $role;
    }



    /**
     * @return array
     */
    public function getRoles():array
    {
        return self::ROLES;
    }

    /**
     * @return array
     */
    public static function getRolesStatic():array
    {
        return self::ROLES;
    }

    /**
     * @return string
     */
    public function getTextPassword(): string
    {
        return $this->textPassword;
    }

    /**
     * @param string $textPassword
     */
    public function setTextPassword(string $textPassword): void
    {
        $this->textPassword = $textPassword;
    }


    /**
     * @return string
     */
    public function getSalt():string
    {
        return '';
    }

    public function eraseCredentials():void
    {
    }

}
