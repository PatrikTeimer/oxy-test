##Prerequisites
* PHP 7.2 or higher
* Apache
* MySql 5.7
* Composer
## Installation

### Step 1: Download 
``` bash
$ git clone git@bitbucket.org:PatrikTeimer/oxy-test.git
```
### Step 2: Install packages
``` bash
$ composer install
```

### Step 3: Create and configure database
``` bash
# Configure DATABASE_URL in .env
DATABASE_URL=mysql://user:password@localhost:8889/name
```
``` bash
# Create database
$ php bin/console doctrine:database:create
```
``` bash
# Create table
$ php bin/console make:migration
```
``` bash
# Run the migration
$ php bin/console doctrine:migrations:migrate
```
## Demo application

### Step1: Create user with HTML form

`GET /create-user`

### Step2: Create user with JSON API

`POST /api/user/create`

**Parameters**

* fullName (string)
* username (string) alias for email
* role (string), allowed: ADMIN, USER
* textPassword (string)

**Example request body**
``` json
{
	"fullName": "Patrik Teimer",
	"username": "patrik@teimer.cz",
	"role": "ADMIN",
	"textPassword": "SuperTajneHeslo_123"
}
```

### Step3: Show users

`GET /api/users`